﻿using AMTSConnection.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMTSConnection {
	public class TabletAdapter {

		private IServer Server;

		public readonly int Port;
		public event Func<BehandlungStatusChangeRequest, int, Response<BehandlungInfo>> BehandlungStatusChangeRequestReceived;
		public event Func<StandardBegründungenRequest, int, Response<StandardBegründungen>> StandardBegründungenRequestReceived;
		public event Action<string, int> TextMessageReceived = (text, tabletID) => { };
		public event Action<int> TabletConnected = (tabletID) => { };
		public event Action<int> TabletDisconnected = (tabletID) => { };
		public IEnumerable<int> ConnectedTablets => Server.EnumerateTabletIDs();

		public TabletAdapter(int port) {
			Port = port;
			Server = new ThreadedServer(port);
			Server.MessageReceived += Server_MessageReceived;
			Server.TabletConnected += (tID) => TabletConnected(tID);
			Server.TabletDisconnected += (tID) => TabletDisconnected(tID);
		}
		~TabletAdapter() {
			Server.Stop();
		}
		public void Start() {
			Server.Start();
		}
		public void Stop() {
			Server.Stop();
		}
		public void Send(BaseMessage message, int tabletID) => Server.Send(Util.Serialize(message), tabletID);

		public void Broadcast(BaseMessage message) => Server.Broadcast(Util.Serialize(message));

		private void Server_MessageReceived(object message, int tabletID) {
			if (message is BehandlungStatusChangeRequest bsc) {
				if (BehandlungStatusChangeRequestReceived == null) return;
				Response r = BehandlungStatusChangeRequestReceived(bsc, tabletID);
				Send(r, tabletID);
			} else if (message is TextMessage tm) {
				TextMessageReceived(tm.Text, tabletID);
			} else if (message is StandardBegründungenRequest sbr) {
				if (StandardBegründungenRequestReceived == null) return;
				Response<StandardBegründungen> r = StandardBegründungenRequestReceived(sbr, tabletID);
				Send(r, tabletID);
			}
		}

	}
}
