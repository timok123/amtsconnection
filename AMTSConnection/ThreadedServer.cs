﻿using AMTSConnection.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AMTSConnection {
	class ThreadedServer : IServer {

		public readonly int Port;
		private TcpListener Listener;
		private Thread listeningThread;
		private bool isListening;
		private List<Client> Clients = new List<Client>();

		public event Action<object, int> MessageReceived;
		public event Action<int> TabletConnected;
		public event Action<int> TabletDisconnected;

		public TimeSpan ClientCheckInterval = new TimeSpan(0, 0, 10);

		public ThreadedServer(int port) {
			Port = port;
			Thread checkThread = new Thread(() => {
				while (true) {
					//Console.WriteLine("Perform Client Check");
					for (int i = 0; i < Clients.Count; i++) {
						Client c = Clients[i];
						if (!c.Send(Util.Serialize(new ControlMessage(ControlCode.Ping)))) {

						}
					}
					Thread.Sleep(ClientCheckInterval);
				}
			});
			checkThread.Start();
		}
		public void Start() {
			listeningThread = new Thread(() => {
				Listener = new TcpListener(IPAddress.Any, Port);
				Listener.Start();
				Console.WriteLine("[ThreadedServer] Lauschen gestartet.");
				isListening = true;
				while (isListening) {
					var tcpClient = Listener.AcceptTcpClient();
					if (Util.ServerHandshake(tcpClient.GetStream(), out int tabletID)) {
						lock (Clients) {
							Client newClient = new Client(tcpClient, tabletID, On_Tablet_Connected, On_Message_Received, On_Connection_Error);
							var existingClient = Clients.FirstOrDefault(c => c.TabletID == newClient.TabletID);
							if (existingClient != null) {
								Clients.Remove(existingClient);
								existingClient.Dispose();
							}
							Clients.Add(newClient);
						}
					} else {
						Console.WriteLine("Handshake mit Tablet fehlgeschlagen.");
					}
				}
				Listener.Stop();
				Console.WriteLine("[ThreadedServer] Lauschen beendet.");
			}) {
				Name = "Listening Thread"
			};
			listeningThread.Start();
		}

		private void On_Connection_Error(Client client, string errorMessage) {
			Console.WriteLine("Fehler bei Tablet " + client.TabletID + ": " + errorMessage);
			DisconnectClient(client);
		}

		private void On_Message_Received(byte[] data, Client client) {
			object message = Util.Deserialize(data);
			if (message is ControlMessage cMsg) {
				switch (cMsg.Code) {
					case ControlCode.Disconnect: DisconnectClient(client); break;
					case ControlCode.Ping: break;
				}
			} else {
				MessageReceived.Invoke(message, client.TabletID);
			}
		}

		private void DisconnectClient(Client client) {
			int tID = client.TabletID;
			lock (Clients) {
				Clients.Remove(client);
			}
			client.Dispose();
			client = null;
			TabletDisconnected(tID);
		}

		private void On_Tablet_Connected(Client client) {
			TabletConnected.Invoke(client.TabletID);
		}

		public void Broadcast(byte[] message) {
			lock (Clients) {
				Clients.ForEach(c => c.Send(message));
			}
		}

		public void Send(byte[] message, int tabletID) {
			if (Clients.FirstOrDefault(c => c.TabletID == tabletID) is Client client) {
				client.Send(message);
			} else {
				Console.WriteLine("Fehler: Konnte Tablet mit ID " + tabletID + " nicht finden.");
			}
		}

		public IEnumerable<int> EnumerateTabletIDs() {
			return Clients.Select(cl => cl.TabletID);
		}

		public void Stop() {
			// Listening-Thread beenden.
			isListening = false;
			using (TcpClient tmpClient = new TcpClient()) {
				tmpClient.Connect("localhost", Port);
				byte[] buffer = Encoding.UTF8.GetBytes("disconnect");
				tmpClient.GetStream().Write(buffer, 0, buffer.Length);
				tmpClient.Close();
			}
			// Alle offenen Verbindungen zu den Clients beenden.
			while (Clients.Count > 0) {
				DisconnectClient(Clients[0]);
			}

		}

		private class Client : IDisposable {
			private Action<Client> ConnectionEstablished;
			private Action<byte[], Client> MessageReceived;
			private Action<Client, string> ConnectionError;

			public TcpClient MyTcpClient { get; private set; }
			public bool Running;
			public readonly int TabletID;
			private Thread MyThread;

			public Client(TcpClient tcpClient, int tabletID, Action<Client> on_tablet_connected, Action<byte[], Client> on_message_received, Action<Client, string> on_connection_error) {
				MyTcpClient = tcpClient;
				TabletID = tabletID;
				ConnectionEstablished = on_tablet_connected;
				MessageReceived = on_message_received;
				ConnectionError = on_connection_error;
				Running = true;
				MyThread = new Thread(NetworkReceive) {
					Name = "Client Thread " + TabletID,
				};
				MyThread.Start();
			}


			public bool Send(byte[] data) {
				try {
					Util.SendMessage(MyTcpClient.GetStream(), data);
				} catch (Exception e) {
					ConnectionError(this, e.Message);
					return false;
				}
				return true;
			}
			private void NetworkReceive() {
				ConnectionEstablished(this);
				while (Running) {
					try {
						var data = Util.ReadMessage(MyTcpClient.GetStream());
						MessageReceived(data, this);
					} catch (Exception e) {
						ConnectionError(this, e.Message);
						Running = false;
					}
				}
			}

			public void Dispose() {
				Running = false;
				MyTcpClient.Close();
				MyTcpClient.Dispose();
			}
		}
	}
}
