﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMTSConnection {
	class Constants {
		public static string HANDSHAKE_OK = "OK ",
			HANDSHAKE_CORRECTION = "Correction ",
			HANDSHAKE_HELLO = "Hello ";
	}
}
