﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AMTSConnection {
	class Util {

		public static readonly int ReadInterval = 10;

		public static byte[] ReadMessage(NetworkStream stream) {
			try {
				BinaryReader br = new BinaryReader(stream);
				int pll = br.ReadInt32();
				return br.ReadBytes(pll);
			} catch (Exception) {
				return null;
			}
		}
		public static void SendMessage(NetworkStream stream, byte[] data) {
			var pllBuffer = BitConverter.GetBytes(data.Length);
			if (pllBuffer.Length != 4) Debugger.Break();
			stream.Write(pllBuffer, 0, pllBuffer.Length);
			stream.Write(data, 0, data.Length);
			
		}
		public static async Task SendMessageAsync(NetworkStream stream, byte[] data) {
			var pllBuffer = BitConverter.GetBytes(data.Length);
			await stream.WriteAsync(pllBuffer, 0, pllBuffer.Length);
			await stream.WriteAsync(data, 0, data.Length);
		}
		public static byte[] Serialize(object anySerializableObject) {
			using (var memStream = new MemoryStream()) {
				(new BinaryFormatter()).Serialize(memStream, anySerializableObject);
				byte[] data = memStream.ToArray();
				return data;
			}
		}
		public static object Deserialize(byte[] message) {
			if (message == null) return null;
			//Log(message);
			using (var memStream = new MemoryStream(message)) {
				return (new BinaryFormatter()).Deserialize(memStream);
			}
		}
		public static bool ServerHandshake(NetworkStream stream, out int tabletID) {
			try {
				var tabIDBytes = Util.ReadMessage(stream);
				tabletID = BitConverter.ToInt32(tabIDBytes, 0);
				Util.SendMessage(stream, Encoding.UTF8.GetBytes(Constants.HANDSHAKE_HELLO + tabletID));
				string helloAck = Encoding.UTF8.GetString(Util.ReadMessage(stream));
				if (helloAck.Equals(Constants.HANDSHAKE_OK + tabletID)) return true;

				if (helloAck.Equals(Constants.HANDSHAKE_CORRECTION + tabletID)) {
					tabletID = int.Parse(helloAck.Substring(Constants.HANDSHAKE_CORRECTION.Length));
					Util.SendMessage(stream, Encoding.UTF8.GetBytes(Constants.HANDSHAKE_HELLO + tabletID));
					return true;
				}
			} catch {
				tabletID = -1;
				return false;
			}
			return false;
		}
		public static bool ClientHandshake(NetworkStream stream, int tabletID) {
			SendMessage(stream, BitConverter.GetBytes(tabletID));
			byte[] tIDAckBytes = ReadMessage(stream);
			string tIDAck = Encoding.UTF8.GetString(tIDAckBytes);
			if (tIDAck.Equals(Constants.HANDSHAKE_HELLO + tabletID)) {
				SendMessage(stream, Encoding.UTF8.GetBytes(Constants.HANDSHAKE_OK + tabletID));
			} else {
				SendMessage(stream, Encoding.UTF8.GetBytes(Constants.HANDSHAKE_CORRECTION + tabletID));
				tIDAckBytes = ReadMessage(stream);
				tIDAck = Encoding.UTF8.GetString(tIDAckBytes);
				if (!tIDAck.Equals(Constants.HANDSHAKE_HELLO + tabletID)) {
					return false;
				}
			}
			return true;
		}

		private static void Log(byte[] data) {
			int n = 1;
			while (File.Exists("./log/log" + n + ".txt")) n++;
			Directory.CreateDirectory("./log");
			File.WriteAllBytes("./log/log" + n + ".txt", data);
			//string str = data.Select(x => x.ToString()).Aggregate((a, b) => a + " " + b);
		}

	}
}
