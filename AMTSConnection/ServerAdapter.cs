﻿using AMTSConnection.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AMTSConnection {
	public class ServerAdapter {

		private bool _connected;
		private TcpClient Client;
		private NetworkStream Stream => Client.GetStream();
		private List<Entry> WaitingMessages = new List<Entry>();
		private Thread NetworkThread;
		public string ServerHost { get; }
		public int ServerPort { get; }
		public int TabletID { get; }
		//public readonly TimeSpan Timeout = TimeSpan.FromMilliseconds(2000);

		public event Action<PatientInfo> PatientReceived = (patient) => { };
		public event Action<BenutzerInfo> UserReceived = (user) => { };
		public event Action<BehandlungStatusChangeRequest> BehandlungStatusChangeReceived = (bscr) => { };
		public event Action<string> TextReceived = (text) => { };
		public event Action ConnectedChanged = () => { };

		public bool Connected {
			get => _connected; private set {
				if (_connected == value) return;
				_connected = value;
				ConnectedChanged?.Invoke();
			}
		}

		public ServerAdapter(string serverHost, int serverPort, int tabletID) {
			ServerHost = serverHost;
			ServerPort = serverPort;
			TabletID = tabletID;
		}
		public void Connect() {
			NetworkThread = new Thread(() => {
				using (Client = new TcpClient()) {
					try {
						Client.Connect(ServerHost, ServerPort);
					} catch { return; }
					if (!Util.ClientHandshake(Stream, TabletID)) {
						// Handshake fehlgeschlagen.
						return;
					}
					// Verbindung erfolgreich hergestellt.
					Connected = true;
					while (Connected) {
						try {
							if (Client == null) break;
							var data = Util.ReadMessage(Stream);
							if (data == null)
								break;
							HandleIncomingMessage(data);
						} catch {
							break;
						}
					}
					Connected = false;
				}
				Client.Close();
			}) {
				IsBackground = true,
				Name = "Network Thread"
			};
			NetworkThread.Start();
		}
		
		public void Disconnect() {
			try {
				Util.SendMessage(Stream, Util.Serialize(new ControlMessage(ControlCode.Disconnect)));
				Client.Close();
			} catch { }
		}
		public void Send(BaseMessage message) {
			try {
				Util.SendMessage(Stream, Util.Serialize(message));
			} catch {
				this.Connected = false;
			}
		}
		public async Task<Response<T>> AskAsync<T>(BaseMessage message) where T : BaseMessage {
			var entry = new Entry(message);
			WaitingMessages.Add(entry);
			await Util.SendMessageAsync(Stream, Util.Serialize(message));
			return entry.TCS.Task.Result as Response<T>;
		}

		public async Task<Response> AskAsync(BaseMessage message) {
			var entry = new Entry(message);
			WaitingMessages.Add(entry);
			await Util.SendMessageAsync(Stream, Util.Serialize(message));
			return entry.TCS.Task.Result;
		}

		private void HandleIncomingMessage(byte[] data) {
			object mo = Util.Deserialize(data);
			if (mo == null) return;
			if (mo is Response resp) {
				if (WaitingMessages.FirstOrDefault(e => e.Message.MessageID.Equals(resp.ReferenceID)) is Entry entry) {
					//Console.WriteLine("Ask Delay: " + Math.Round((DateTime.Now - entry.StartTime).TotalMilliseconds) + " ms");
					WaitingMessages.Remove(entry);
					entry.TCS.SetResult(resp);
				}
			} else {
				if (mo is PatientInfo pa) {
					PatientReceived?.Invoke(pa);
				} else if (mo is BenutzerInfo u) {
					UserReceived?.Invoke(u);
				} else if (mo is TextMessage txt) {
					TextReceived?.Invoke(txt.Text);
				} else if (mo is BehandlungStatusChangeRequest bscr) {
					BehandlungStatusChangeReceived?.Invoke(bscr);
				} else if (mo is ControlMessage ctrlM) {
					Console.WriteLine("ControlMessage Empfangen: " + ctrlM.Code);
					if (ctrlM.Code == ControlCode.Disconnect) {
						Client = null;
					}
				} else {
					throw new NotSupportedException();
				}
			}
		}
		private class Entry {
			public readonly BaseMessage Message;
			public readonly DateTime StartTime = DateTime.Now;
			public readonly TaskCompletionSource<Response> TCS = new TaskCompletionSource<Response>();

			public Entry(BaseMessage message) {
				Message = message;
			}
		}
	}
}
