﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMTSConnection.Model {
	[Serializable]
	public class ControlMessage : BaseMessage {

		public ControlCode Code;

		public ControlMessage(ControlCode code) {
			Code = code;
		}
	}
	public enum ControlCode {
		Disconnect = 0,
		AckDisconnect = 1,
		Ping = 2,
	}
}
