﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMTSConnection.Model {
	[Serializable]
	public class BenutzerInfo : BaseMessage {

		public Guid? BenutzerID;
		public string Username;
		public Guid? MandantID;

		public override string ToString() {
			return Username;
		}
	}
}
