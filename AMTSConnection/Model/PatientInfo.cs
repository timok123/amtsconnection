﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMTSConnection.Model {
	[Serializable]
	public class PatientInfo : BaseMessage {

		public Guid PatientenID;
		public byte[] Image;
		public string FirstName, LastName, Street, ZIP, City;
		public string DateOfBirth;

		public int Zyklus, TagNummer;
		public string TherapieName;

		public BehandlungInfo[] BehandlungenHeute;
	}
}
