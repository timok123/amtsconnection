﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMTSConnection.Model {
	[Serializable]
	public class GespeicherteTexte : BaseMessage {

		public List<string> TexteKurz;
		public List<string> TexteLang;
	}
}
