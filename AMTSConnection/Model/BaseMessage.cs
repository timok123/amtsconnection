﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMTSConnection.Model {
	[Serializable]
	public abstract class BaseMessage {

		public Guid MessageID { get; set; } = Guid.NewGuid();
	}
}
