﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMTSConnection.Model {
	[Serializable]
	public class TextMessage : BaseMessage {

		public string Text;

		public TextMessage(string text) {
			Text = text;
		}
	}
}
