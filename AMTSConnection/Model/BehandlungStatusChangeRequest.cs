﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMTSConnection.Model {
	[Serializable]
	public class BehandlungStatusChangeRequest : BaseMessage {

		public BenutzerInfo VerantwortlicherBenutzer;
		public BehandlungInfo DieBehandlung;
		public PatientInfo Patient;
		public string Benutzerkürzel;
		public string Bemerkung;
	}
}
