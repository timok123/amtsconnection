﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMTSConnection.Model {
	[Serializable]
	public class BehandlungInfo : BaseMessage {

		public Guid PATTHPZLID;
		public Guid ZUBIdentTag;
		public DateTime? StartTime, EndTime;
		public TimeSpan TargetDuration;
		public string Name;
		public bool IsActive;
		public enum BehandlungsStatus {
			Ohne = 0,
			Gestartet = 1,
			Pausiert = 2,
			Abbruch = 3,
			EndeRegulär = 4,
		}
		public BehandlungsStatus Status;
	}
}
