﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMTSConnection.Model {
	[Serializable]
	public class Response<T> : Response where T : BaseMessage {
		public T ResponseMessage;
		
		public Response(ResponseType type, T responseMessage, Guid refID) : base(type, refID) {
			ResponseMessage = responseMessage;
		}
	}
	[Serializable]
	public class Response : BaseMessage {
		public enum ResponseType {
			OK,
			Error,
			Timeout
		}
		public ResponseType Type;
		public Guid ReferenceID;
		public string ResponseText;

		public Response(ResponseType type, Guid refID) {
			Type = type;
			ReferenceID = refID;
		}
		
	}
	
}
