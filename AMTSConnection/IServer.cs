﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMTSConnection {
	interface IServer {
		event Action<object, int> MessageReceived;
		event Action<int> TabletConnected;
		event Action<int> TabletDisconnected;
		IEnumerable<int> EnumerateTabletIDs();

		void Start();
		void Stop();
		void Broadcast(byte[] message);
		void Send(byte[] message, int tabletID);
	}
}
